package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

type route struct {
	ShortName string `json:"shortName"`
}

type trip struct {
	Route route `json:"route"`
}

type stoptimesWithoutPatterns struct {
	Trip               trip   `json:"trip"`
	ServiceDay		   int    `json:"serviceDay`
	ScheduledDeparture int    `json:"scheduledDeparture"`
	RealtimeDeparture  int    `json:"realtimeDeparture"`
	Realtime           bool   `json:"realtime"`
	RealtimeState      string `json:"realtimeState"`
	Headsign           string `json:"headsign"`
}

type stop struct {
	Name                     string                     `json:"name"`
	GtfsID                   string                     `json:"gtfsId"`
	Code                     string                     `json:"code"`
	StoptimesWithoutPatterns []stoptimesWithoutPatterns `json:"stoptimesWithoutPatterns"`
}

type data struct {
	Stop stop `json:"stop"`
}

type jsonData struct {
	Data data `json:"data"`
}

type isotime struct {
	time.Time
}

func (t isotime) MarshalJSON() ([]byte, error) {
    stamp := fmt.Sprintf("\"%s\"", t.Format(time.RFC3339))
    fmt.Println(stamp)
    return []byte(stamp), nil
}

type bus struct {
	Name      string    `json:"name"`
	Departure isotime   `json:"departure"`
	Headsign  string    `json:"headsign"`
}

type jsonMessage struct {
	Buses []*bus
}

type HSL struct {
}

func (hsl *HSL) getRoutes(broadcast chan []byte) {
	q := []byte(`
	{
		stop(id: "HSL:1434114") {
			name
			gtfsId
			code
			stoptimesWithoutPatterns(omitNonPickups:true) {
				trip {
				  route {
					shortName
				  }
				}
				serviceDay
				scheduledDeparture
				realtimeDeparture
				realtime
				realtimeState
				headsign
			}
		}
	}`)

	body := bytes.NewReader(q)

	// rest client

	url := "https://api.digitransit.fi/routing/v1/routers/hsl/index/graphql"

	req, err := http.NewRequest("POST", url, body)
	if err != nil {
		fmt.Println("http client error")
		panic(err)
	}

	req.Header.Add("Content-Type", "application/graphql")

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		log.Println(err)
		return
	}

	resBytes, err := ioutil.ReadAll(res.Body)

	s := &jsonData{}

	err = json.Unmarshal(resBytes, s)
	if err != nil {
		log.Println(err)
		return
	}

	buses := []*bus{}
	
	/* fmt.Println(`Stop: `, it.Name)
	fmt.Println() */
	for _, itt := range s.Data.Stop.StoptimesWithoutPatterns[:4] {

		timestamp := itt.ServiceDay + itt.RealtimeDeparture
        departure := time.Unix(int64(timestamp), 0)

		/* fmt.Println(`Realtime departure: `, departure)
		fmt.Println(`Route: `, itt.Trip.Route.ShortName)
		fmt.Println() */

		//fmt.Println(departure)


		buses = append(buses, &bus{
			Name:      itt.Trip.Route.ShortName,
			Headsign:  itt.Headsign,
			Departure: isotime{departure},
		})
	}
	

	msg := jsonMessage{
		Buses: buses,
	}

	bytes, err := json.Marshal(msg)
	if err != nil {
		log.Fatal(err)
	}

	broadcast <- bytes

}

type WSServer struct {
	client    *websocket.Conn
	broadcast chan []byte
}

func checkOrigin(r *http.Request) bool {
	origin := r.Header.Get("Origin")
	if origin == "http://localhost:8000" || origin == "file://" {
		return true
	}
	return false
}

var upgrader = websocket.Upgrader{
	CheckOrigin: checkOrigin,
}

func (ws *WSServer) connect(w http.ResponseWriter, r *http.Request) {

	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	defer c.Close()

	ws.client = c

	log.Println("client connected")

	for {
		/* _, message, err := c.ReadMessage()
		if err != nil {
			log.Println("read:", err)
			break
		}
		log.Printf("recv: %s", message) */

		select {
		case msg := <-ws.broadcast:
			err := ws.client.WriteMessage(websocket.TextMessage, msg)
			if err != nil {
				// do nothing on ws error
			}
		}
	}

}

func main() {

	ws := &WSServer{
		broadcast: make(chan []byte),
	}

	hsl := &HSL{}

	routeTicker := time.NewTicker(2000 * time.Millisecond)
	go func() {
		for _ = range routeTicker.C {
			hsl.getRoutes(ws.broadcast)
		}
	}()

	// websocket server
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		ws.connect(w, r)
	})

	http.ListenAndServe(":3001", nil)

}
